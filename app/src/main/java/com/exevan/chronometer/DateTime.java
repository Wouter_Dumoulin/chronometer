package com.exevan.chronometer;

/**
 * Created by Exevan on 12/10/2015.
 */
public class DateTime {

    private final long millis;

    public DateTime(long currentTime) {
        millis = currentTime;
    }

    public long getTime() {
        return millis;
    }

    public static DateTime NOW() {
        return new DateTime(System.currentTimeMillis());
    }

    public TimeSpan difference(DateTime dateTime)
    {
        return new TimeSpan(this.millis - dateTime.millis);
    }
}
