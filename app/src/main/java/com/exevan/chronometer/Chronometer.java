package com.exevan.chronometer;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.SharedPreferences;


public class Chronometer extends Activity {

    private SharedPreferences data;
    private Handler handler;

    private TextView timeView;
    private Button startStopButton;
    private Button resetButton;

    private TimeSpan accumulatedTime;
    private DateTime currentTime;
    private boolean isRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        accumulatedTime = TimeSpan.ZERO;

        setContentView(R.layout.activity_chronometer);
        timeView = (TextView) findViewById(R.id.timeView);

        startStopButton = (Button) findViewById(R.id.startStopButton);
        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isRunning) {
                    isRunning = false;
                    startStopButton.setText(R.string.start);
                } else {
                    isRunning = true;
                    startStopButton.setText(R.string.stop);
                    currentTime = DateTime.NOW();
                    generateDelayedTick();
                }
            }
        });

        resetButton = (Button) findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

        data  = getSharedPreferences("chronometerdata", MODE_PRIVATE);
        handler = new Handler();
        showTime();

        generateDelayedTick();
    }


    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences.Editor dataEditor = data.edit();
        dataEditor.putLong("accumulatedTime", accumulatedTime.getTime());
        dataEditor.putLong("currentTime", currentTime.getTime());
        dataEditor.putBoolean("isRunning", isRunning);
        dataEditor.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        accumulatedTime = new TimeSpan(data.getLong("accumulatedTime", 0));
        currentTime = new DateTime(data.getLong("currentTime", 0));
        isRunning = data.getBoolean("isRunning", false);
        showTime();
    }

    private void showTime() {
        timeView.setText(String.valueOf(accumulatedTime.toString()));
    }

    private void increment() {
        DateTime now = DateTime.NOW();
        TimeSpan newTime = now.difference(currentTime);
        accumulatedTime = accumulatedTime.add(newTime);
        currentTime = now;
        showTime();
    }

    private void reset() {
        isRunning = false;
        accumulatedTime = TimeSpan.ZERO;
        showTime();
    }

    private void generateDelayedTick() {
        handler.postDelayed(onTick, 25);
    }

    Runnable onTick = new Runnable() {
        @Override
        public void run() {
            if(isRunning) {
                increment();
                generateDelayedTick();
            }
        }
    };
}
