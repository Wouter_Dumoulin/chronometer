package com.exevan.chronometer;

/**
 * Created by Exevan on 12/10/2015.
 */
public class TimeSpan {

    private final long millis;

    public TimeSpan(long millis)
    {
        this.millis = millis;
    }

    public TimeSpan add(TimeSpan timeSpan) {
        return new TimeSpan(timeSpan.millis + millis);
    }

    public long getTime() {
        return millis;
    }

    public int getMillis() {
        return (int)  millis % 1000;
    }

    public int getSeconds() {
        return (int) (millis / 1000) % 60;
    }

    public int getMinutes() {
        return (int) ((millis / (1000*60)) % 60);
    }

    public int getHours() {
        return (int) ((millis / (1000*60*60)) % 24);
    }

    @Override
    public String toString() {
        return getHours() + ":" + getMinutes() + ":" + getSeconds() + ":" + getMillis();
    }

    public static final TimeSpan ZERO = new TimeSpan(0);
}
